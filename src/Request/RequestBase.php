<?php

namespace MzSms\Request;

use MzSms\SmsSdk;

abstract class RequestBase
{
    protected SmsSdk $smsSdk;
    protected string $apiPath;

    public function __construct(SmsSdk $smsSdk)
    {
        $this->smsSdk = $smsSdk;
    }

    public abstract function request();
}
