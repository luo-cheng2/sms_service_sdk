<?php

namespace MzSms\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use MzSms\Request\RequestBase;
use MzSms\Response\Response;
use MzSms\SmsSdk;

class SingleSend extends RequestBase
{
    private string $mobile;
    private int $templateId;
    private array $params;

    public function setMobile($mobile): self
    {
        if (!is_string($mobile)) {
            $mobile = (string)$mobile;
        }
        $this->mobile = $mobile;
        return $this;
    }

    public function setTemplateId($templateId): self
    {
        if (!is_numeric($templateId)) {
            throw new \Exception("参数格式不正确", 500);
        }
        if (is_string($templateId)) {
            $templateId = (int)$templateId;
        }
        $this->templateId = $templateId;
        return $this;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;
        return $this;
    }

    function __construct(SmsSdk $smsSdk)
    {
        parent::__construct($smsSdk);
        $this->apiPath = "/sdk/send";
    }

    private function checkParams()
    {
        if (empty($this->params)) {
            throw new \Exception("缺少 params 参数", 500);
        }
        if (empty($this->mobile)) {
            throw new \Exception("缺少 mobile 手机号", 500);
        }
        if (empty($this->templateId)) {
            throw new \Exception("缺少 templateId 模板编号", 500);
        }
    }

    public function request(): Response
    {
        $this->checkParams();
        try {
            $client = new Client();
            $response = $client->post($this->smsSdk->getHost() . $this->apiPath, [
                'timeout' => $this->smsSdk->getTimeout(),
                'headers' => [
                    'app-code' => $this->smsSdk->getAppCode(),
                ],
                'form_params' => [
                    'mobile' => $this->mobile,
                    'template_id' => $this->templateId,
                    'params' => $this->params,
                ],
            ]);
            $content =  $response->getBody()->getContents();
            return new Response($content);
        } catch (GuzzleException $e) {
            throw $e;
        }
    }
}
