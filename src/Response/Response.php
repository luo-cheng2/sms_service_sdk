<?php

namespace MzSms\Response;

class Response
{

    const CODE_SUCCESS = 0;

    private int $code = 0;
    private array $data = [];
    private string $msg = '';
    private int $page = 1;
    private int $pageSize = 10;
    private int $total = 0;

    public function getMsg()
    {
        return $this->msg;
    }

    public function setMsg($msg): self
    {
        $this->msg = $msg;
        return $this;
    }

    public function getCode(): int
    {
        return $this->code;
    }
    public function setCode(int $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }
    public function getPageSize(): int
    {
        return $this->pageSize;
    }
    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
    public function setTotal(int $total): self
    {
        $this->total = $total;
        return $this;
    }

    public function isSuccess(): bool
    {
        if ($this->code == self::CODE_SUCCESS) {
            return true;
        }
        return false;
    }

    public function __construct($response)
    {
        if (is_string($response)) {
            $response = json_decode($response, true);
        }

        $this->code = $response["code"];
        $this->msg = $response["msg"] ?? '';
        $this->data = $response["data"] ?? [];
        $this->page = $response["page"] ?? 1;
        $this->pageSize = $response["pageSize"] ?? 10;
        $this->total = $response["total"] ?? 0;
    }
}
