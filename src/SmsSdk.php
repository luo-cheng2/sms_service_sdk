<?php

namespace MzSms;

use MzSms\Request\BatchSend;
use MzSms\Request\SingleSend;
use think\facade\Config;

class SmsSdk
{

    protected string $host = "";
    protected string $appCode = "";
    protected int $timeout = 1;

    public function getHost(): string
    {
        return $this->host;
    }
    public function getAppCode(): string
    {
        return $this->appCode;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function __construct(array $config)
    {
        if (empty($config["host"])) {
            throw new \Exception("缺少host", 500);
        }
        if (empty($config["appCode"])) {
            throw new \Exception("缺少appCode", 500);
        }
        if (!empty($config["timeout"])) {
            $this->timeout = $config["timeout"];
        }

        $this->host = $config["host"];
        $this->appCode = $config["appCode"];
    }

    public function singleSend(): SingleSend
    {
        return new SingleSend($this);
    }

    public function batchSend(): BatchSend
    {
        return new BatchSend($this);
    }
}
